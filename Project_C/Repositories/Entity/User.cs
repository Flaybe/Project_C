﻿using Repositories.Entity.UtilityObject;
using System.Security.Cryptography;

namespace Repositories.Entity
{
    public class User : IdentityObject
    {
        public string Login { get; set; }
        private string _password;
        public string Password
        {
            get => _password;
            set => _password = HashPassword(value);
        }

        public static string HashPassword(string password)
        {
            // Generate a random salt
            byte[] salt = new byte[16];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            // Hash the password with the salt and 10000 iterations
            byte[] hash = new Rfc2898DeriveBytes(password, salt, 10000).GetBytes(32);

            // Combine the salt and hash into a single string
            byte[] hashBytes = new byte[48];
            Buffer.BlockCopy(salt, 0, hashBytes, 0, 16);
            Buffer.BlockCopy(hash, 0, hashBytes, 16, 32);
            string hashedPassword = Convert.ToBase64String(hashBytes);

            return hashedPassword;
        }

        public static bool VerifyPassword(string password, string hashedPassword)
        {
            // Extract the salt and hash from the stored hash
            byte[] hashBytes = Convert.FromBase64String(hashedPassword);
            byte[] salt = new byte[16];
            Buffer.BlockCopy(hashBytes, 0, salt, 0, 16);
            byte[] hash = new byte[32];
            Buffer.BlockCopy(hashBytes, 16, hash, 0, 32);

            // Hash the entered password with the extracted salt and 10000 iterations
            byte[] enteredHash = new Rfc2898DeriveBytes(password, salt, 10000).GetBytes(32);

            // Compare the two hashes to verify the password
            for (int i = 0; i < 32; i++)
            {
                if (hash[i] != enteredHash[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
