﻿using Repositories.Entity.UtilityObject;

namespace Repositories.Entity
{
    public class Diploma : IdentityObject
    {
        public required string Name { get; set; }
        public string? Description { get; set; }
    }
}
