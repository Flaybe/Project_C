﻿using Repositories.Entity.UtilityObject;

namespace Repositories.Entity
{
    public class Experience : IdentityObject
    {
        public required string CompanyName { get; set; }
        public string? Description { get; set; }
    }
}
