﻿using Repositories.Entity.UtilityObject;

namespace Repositories.Entity
{
    public class Skill : IdentityObject
    {
        public required string Name { get; set; }

        public int Rating { get; set; }

        public Guid SkillCategoryId { get; set; }
        public SkillCategory SkillCategory { get; set; }
    }
}
