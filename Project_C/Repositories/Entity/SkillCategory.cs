﻿using Repositories.Entity.UtilityObject;

namespace Repositories.Entity
{
    public class SkillCategory : IdentityObject
    {
        public required string Name { get; set; }

        public List<Skill>? Skills { get; set; }
    }
}
