﻿using Repositories.Entity.UtilityObject;

namespace Repositories.Entity
{
    public class Passion : IdentityObject
    {
        public required string Title { get; set; }
        public string? Description { get; set; }
        
        public LocalFile? File { get; set; }
    }
}
