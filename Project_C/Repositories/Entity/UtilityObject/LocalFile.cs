﻿using Microsoft.AspNetCore.Http;
using Repositories.Enumerations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Entity.UtilityObject
{
    public class LocalFile : IdentityObject
    {
        [NotMapped]
        public IFormFile FileDetails { get; set; }
        public string? FileName { get; set; }
        public byte[]? FileData { get; set; }
        public FileType FileType { get; set; }
    }
}
