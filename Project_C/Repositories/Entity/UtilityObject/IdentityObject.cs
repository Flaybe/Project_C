﻿namespace Repositories.Entity.UtilityObject
{
    public class IdentityObject
    {
        private Guid _id;
        public Guid Id
        {
            get => _id;
            set => _id = Guid.NewGuid();
        }
    }
}
