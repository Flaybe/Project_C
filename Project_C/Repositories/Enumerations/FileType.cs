﻿namespace Repositories.Enumerations
{
    public enum FileType
    {
        PDF=1,
        JPG=2,
        PNG=3,
    }
}
