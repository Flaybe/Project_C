﻿using Microsoft.EntityFrameworkCore;
using Repositories.Entity;
using Repositories.Entity.UtilityObject;
using Repositories.Interfaces;
using System.Linq.Expressions;

namespace Repositories.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly CvDb _context;
        private DbSet<User> _table;
        public UserRepository(CvDb context)
        {
            _context = context;
            _table = context.Set<User>();
        }
        private void Save()
        {
            _context.SaveChanges();
        }

        public User Add(User obj)
        {
            var res = _table.Add(obj).Entity;
            Save();
            return res;
        }

        public bool Remove(Guid id)
        {
            var user = _table.Find(id);
            if (user == null)
                return false;
            _table.Remove(user);
            Save();
            return true;
        }

        public User? Get(Guid id)
        {
            var res = _table.Where(x => x.Id == id).AsQueryable();
            foreach (var property in typeof(User).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;
                res.Include(property.Name).Load();
            }
            return res.FirstOrDefault();
        }

        public List<User> GetAll(Expression<Func<User, bool>> condition)
        {
            condition ??= x => true;
            var res = _table.Where(condition).AsQueryable();
            foreach (var property in typeof(User).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;

                res.Include(property.Name).Load();
            }

            return res.ToList();
        }

        public User? Update(Guid id, User updatedObj)
        {
            if (id != updatedObj.Id)
                return null;

            var existingObj = _table.Find(id);
            if (existingObj == null)
                return null;

            _context.Entry(existingObj).CurrentValues.SetValues(updatedObj);

            Save();
            return existingObj;
        }

        public int Count(Expression<Func<User, bool>> condition)
        {
            condition ??= x => true;
            return _table.Count(condition);
        }

        public bool Exists(Guid id) =>
            _table.Any(e => e.Id == id);
    }
}
