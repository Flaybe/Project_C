﻿using Microsoft.EntityFrameworkCore;
using Repositories.Entity;
using Repositories.Entity.UtilityObject;
using Repositories.Interfaces;
using System.Linq.Expressions;

namespace Repositories.Repositories
{
    public class SkillRepository : IRepository<Skill>, ISkillRepository
    {
        private readonly CvDb _context;
        private DbSet<Skill> _table;

        public SkillRepository(CvDb context)
        {
            _context = context;
            _table = context.Set<Skill>();
        }

        private void Save()
        {
            _context.SaveChanges();
        }

        public Skill Add(Skill obj)
        {
            var res = _table.Add(obj).Entity;

            Save();
            return res;
        }

        public int Count(Expression<Func<Skill, bool>> condition)
        {
            condition ??= x => true;
            return _table.Count(condition);
        }

        public bool Exists(Guid id) =>
             _table.Any(e => e.Id == id);

        public Skill? Get(Guid id)
        {
            var res = _table.Where(x => x.Id == id).AsQueryable();
            foreach (var property in typeof(Skill).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;
                res.Include(property.Name).Load();
            }
            return res.FirstOrDefault();
        }

        public List<Skill> GetAll(Expression<Func<Skill, bool>> condition)
        {
            condition ??= x => true;
            var res = _table.Where(condition).AsQueryable();
            foreach (var property in typeof(Skill).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;

                res.Include(property.Name).Load();
            }

            return res.ToList();
        }

        public bool Remove(Guid id)
        {
            var passion = _table.Find(id);
            if (passion == null)
                return false;
            _table.Remove(passion);
            Save();
            return true;
        }

        public Skill? Update(Guid id, Skill updatedObj)
        {
            if (id != updatedObj.Id)
                return null;

            var existingObj = _table.Find(id);
            if (existingObj == null)
                return null;

            _context.Entry(existingObj).CurrentValues.SetValues(updatedObj);

            Save();
            return existingObj;
        }

        public Skill AddSkillToCategory(Guid SkillCategoryId, Skill skill)
        {
            var category = _context.SkillCategories.FirstOrDefault(sc => sc.Id == SkillCategoryId);

            if(category == null)
                throw new Exception("SkillCategory not found.");
            
            skill.SkillCategory = category;
            skill.SkillCategoryId = SkillCategoryId;

            var res = _table.Add(skill).Entity;

            Save();

            return res;
        }
    }
}
