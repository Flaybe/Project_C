﻿using Microsoft.EntityFrameworkCore;
using Repositories.Entity.UtilityObject;
using Repositories.Interfaces;
using System.Linq.Expressions;

namespace Repositories.Repositories
{
    public class FileRepository : IRepository<LocalFile>
    {
        private readonly CvDb _context;
        private DbSet<LocalFile> _table;
        public FileRepository(CvDb context)
        {
            _context = context;
            _table = context.Set<LocalFile>();
        }
        private void Save()
        {
            _context.SaveChanges();
        }

        public LocalFile Add(LocalFile obj)
        {
            var res = _table.Add(obj).Entity;
            Save();
            return res;
        }

        public int Count(Expression<Func<LocalFile, bool>> condition)
        {
            condition ??= x => true;
            return _table.Count(condition);
        }

        public bool Exists(Guid id) =>
            _table.Any(f => f.Id == id);

        public LocalFile? Get(Guid id)
        {
            var res = _table.Where(x => x.Id == id).AsQueryable();
            foreach (var property in typeof(LocalFile).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;
                res.Include(property.Name).Load();
            }
            return res.FirstOrDefault();
        }

        public List<LocalFile> GetAll(Expression<Func<LocalFile, bool>> condition)
        {
            condition ??= x => true;
            var res = _table.Where(condition).AsQueryable();
            foreach (var property in typeof(LocalFile).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;

                res.Include(property.Name).Load();
            }

            return res.ToList();
        }

        public bool Remove(Guid id)
        {
            var file = _table.Find(id);
            if (file == null)
                return false;
            _table.Remove(file);
            Save();
            return true;
        }

        public LocalFile? Update(Guid id, LocalFile updatedObj)
        {
            if (id != updatedObj.Id)
                return null;

            var existingObj = _table.Find(id);
            if (existingObj == null)
                return null;

            _context.Entry(existingObj).CurrentValues.SetValues(updatedObj);

            Save();
            return existingObj;
        }
    }
}
