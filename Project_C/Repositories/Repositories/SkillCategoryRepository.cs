﻿using Microsoft.EntityFrameworkCore;
using Repositories.Entity;
using Repositories.Entity.UtilityObject;
using Repositories.Interfaces;
using System.Linq.Expressions;
using System.Reflection;

namespace Repositories.Repositories
{
    public class SkillCategoryRepository : IRepository<SkillCategory>
    {

        private readonly CvDb _context;
        private DbSet<SkillCategory> _table;

        public SkillCategoryRepository(CvDb context)
        {
            _context = context;
            _table = context.Set<SkillCategory>();
        }
        private void Save()
        {
            _context.SaveChanges();
        }

        public SkillCategory Add(SkillCategory obj)
        {
            var res = _table.Add(obj).Entity;

            Save();
            return res;
        }

        public int Count(Expression<Func<SkillCategory, bool>> condition)
        {
            condition ??= x => true;
            return _table.Count(condition);
        }

        public bool Exists(Guid id) =>
             _table.Any(e => e.Id == id);

        public SkillCategory? Get(Guid id)
        {
            var res = _table.Where(x => x.Id == id).AsQueryable();
            foreach (var property in typeof(SkillCategory).GetProperties())
            {
                if(property.PropertyType == typeof(List<Skill>))
                {
                    res.Include(property.Name).Load();
                }
            }
            return res.FirstOrDefault();
        }

        public List<SkillCategory> GetAll(Expression<Func<SkillCategory, bool>> condition)
        {
            condition ??= x => true;
            var res = _table.Where(condition).AsQueryable();
            foreach (var property in typeof(SkillCategory).GetProperties())
            {
                if (property.PropertyType == typeof(List<Skill>))
                {
                    res.Include(property.Name).Load();
                }
            }

            return res.ToList();
        }

        public bool Remove(Guid id)
        {
            var passion = _table.Find(id);
            if (passion == null)
                return false;
            _table.Remove(passion);
            Save();
            return true;
        }

        public SkillCategory? Update(Guid id, SkillCategory updatedObj)
        {
            if (id != updatedObj.Id)
                return null;

            var existingObj = _table.Find(id);
            if (existingObj == null)
                return null;

            _context.Entry(existingObj).CurrentValues.SetValues(updatedObj);

            Save();
            return existingObj;
        }
    }
}
