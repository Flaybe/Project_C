﻿using Microsoft.EntityFrameworkCore;
using Repositories.Entity;
using Repositories.Entity.UtilityObject;
using Repositories.Interfaces;
using System.Linq.Expressions;

namespace Repositories.Repositories
{
    public class ExperienceRepository : IRepository<Experience>
    {
        private readonly CvDb _context;
        private DbSet<Experience> _table;

        public ExperienceRepository(CvDb context)
        {
            _context = context;
            _table = context.Set<Experience>();
        }
        private void Save()
        {
            _context.SaveChanges();
        }
        public Experience Add(Experience obj)
        {
            var res = _table.Add(obj).Entity;

            Save();
            return res;
        }

        public int Count(Expression<Func<Experience, bool>> condition)
        {
            condition ??= x => true;
            return _table.Count(condition);
        }

        public bool Exists(Guid id) =>
           _table.Any(e => e.Id == id);

        public Experience? Get(Guid id)
        {
            var res = _table.Where(x => x.Id == id).AsQueryable();
            foreach (var property in typeof(Experience).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;
                res.Include(property.Name).Load();
            }
            return res.FirstOrDefault();
        }

        public List<Experience> GetAll(Expression<Func<Experience, bool>> condition)
        {
            condition ??= x => true;
            var res = _table.Where(condition).AsQueryable();
            foreach (var property in typeof(Experience).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;

                res.Include(property.Name).Load();
            }

            return res.ToList();
        }

        public bool Remove(Guid id)
        {
            var passion = _table.Find(id);
            if (passion == null)
                return false;
            _table.Remove(passion);
            Save();
            return true;
        }

        public Experience? Update(Guid id, Experience updatedObj)
        {
            if (id != updatedObj.Id)
                return null;

            var existingObj = _table.Find(id);
            if (existingObj == null)
                return null;

            _context.Entry(existingObj).CurrentValues.SetValues(updatedObj);

            Save();
            return existingObj; ;
        }
    }
}
