﻿using Microsoft.EntityFrameworkCore;
using Repositories.Entity;
using Repositories.Entity.UtilityObject;
using Repositories.Interfaces;
using System.Linq.Expressions;

namespace Repositories.Repositories
{
    public  class PassionRepository : IRepository<Passion>
    {
        private readonly CvDb _context;
        private DbSet<Passion> _table;
        public PassionRepository(CvDb context)
        {
            _context = context;
            _table = context.Set<Passion>();
        }
        private void Save()
        {
            _context.SaveChanges();
        }

        public Passion Add(Passion obj)
        {
            if(obj.File?.FileDetails != null)
            {
                var file = new LocalFile()
                {
                    FileName = obj.File.FileDetails.FileName,
                };

                using (var stream = new MemoryStream())
                {
                    obj.File.FileDetails.CopyTo(stream);
                    file.FileData = stream.ToArray();
                }
                obj.File = file;
            }
            var res = _table.Add(obj).Entity;

            Save();
            return res;
        }

        public bool Remove(Guid id)
        {
            var passion = _table.Find(id);
            if (passion == null)
                return false;
            _table.Remove(passion);
            Save();
            return true;
        }

        public Passion? Get(Guid id)
        {
            var res = _table.Where(x => x.Id == id).AsQueryable();
            foreach (var property in typeof(Passion).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;
                res.Include(property.Name).Load();
            }
            return res.FirstOrDefault();
        }

        public List<Passion> GetAll(Expression<Func<Passion, bool>> condition)
        {
            condition ??= x => true;
            var res = _table.Where(condition).AsQueryable();
            foreach (var property in typeof(Passion).GetProperties()
                         .Where(x => x.PropertyType.IsSubclassOf(typeof(IdentityObject)))
                         .ToList())
            {
                var dbType = _context.Model.FindEntityType(property.PropertyType);
                if (dbType == null) continue;

                res.Include(property.Name).Load();
            }

            return res.ToList();
        }

        public Passion? Update(Guid id, Passion updatedObj)
        {
            if (id != updatedObj.Id)
                return null;

            var existingObj = _table.Find(id);
            if (existingObj == null)
                return null;

            _context.Entry(existingObj).CurrentValues.SetValues(updatedObj);

            Save();
            return existingObj;
        }

        public int Count(Expression<Func<Passion, bool>> condition)
        {
            condition ??= x => true;
            return _table.Count(condition);
        }

        public bool Exists(Guid id) =>
            _table.Any(e => e.Id == id);
    }
}
