﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Repositories.Entity;
using Repositories.Entity.UtilityObject;
using Repositories.Enumerations;

namespace Repositories
{
    public class CvDb : DbContext
    {
        public CvDb(DbContextOptions<CvDb> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<LocalFile> LocalFiles { get; set; }
        public DbSet<Passion> Passions { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<SkillCategory> SkillCategories { get; set; }
        public DbSet<Diploma> Diplomas { get; set; }
        public DbSet<Experience> Experiences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LocalFile>().Property(x => x.FileType).HasConversion(new EnumToStringConverter<FileType>());
            modelBuilder.Entity<Skill>()
                .HasOne(s => s.SkillCategory)
                .WithMany(c => c.Skills)
                .HasForeignKey(s => s.SkillCategoryId);

        }
    }
}
