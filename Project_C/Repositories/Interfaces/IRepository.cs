﻿using Repositories.Entity.UtilityObject;
using System.Linq.Expressions;

namespace Repositories.Interfaces
{
    public interface IRepository<T> where T : IdentityObject
    {
        T Add(T obj);
        bool Remove(Guid id);
        T? Get(Guid id);
        List<T> GetAll(Expression<Func<T, bool>> condition);
        T? Update(Guid id, T updatedObj);
        int Count(Expression<Func<T, bool>> condition);
        bool Exists(Guid id);
    }
}
