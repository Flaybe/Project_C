﻿using Repositories.Entity;

namespace Repositories.Interfaces
{
    public interface ISkillRepository
    {
        public Skill AddSkillToCategory(Guid SkillCategoryId, Skill skill);
    }
}
