﻿using Microsoft.AspNetCore.Mvc;
using Repositories.Entity;
using Repositories.Interfaces;
using Repositories.Repositories;
using Services.Security;
using System.Linq.Expressions;

namespace Project_C.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeJwt]
    public class SkillController : ControllerBase
    {
        private readonly IRepository<Skill> _skillRepository;
        private readonly ISkillRepository _repository;


        public SkillController(IRepository<Skill> skillRepository, ISkillRepository repository)
        {
            _skillRepository = skillRepository;
            _repository = repository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            return Ok(_skillRepository.Get(id));
        }

        [HttpGet("")]
        public IActionResult GetAll(string? name = null)
        {
            Expression<Func<Skill, bool>> condition = x => true;

            if (name != null)
                condition = x => x.Name.Contains(name);

            return Ok(_skillRepository.GetAll(condition));
        }

        [HttpGet("count")]
        public IActionResult Count()
        {
            Expression<Func<Skill, bool>> condition = x => true;

            return Ok(_skillRepository.Count(condition));
        }

        [HttpGet("Exist/{id}")]
        public IActionResult Exists(Guid id)
        {
            return Ok(_skillRepository.Exists(id));
        }

        [HttpPost("")]
        public IActionResult Add([FromForm] Skill skill)
        {
            return Ok(_skillRepository.Add(skill));
        }

        [HttpPost("{categoryId}")]
        public IActionResult AddSkillToCategory(Guid categoryId, [FromForm] Skill skill)
        {
            return Ok(_repository.AddSkillToCategory(categoryId, skill));
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(Guid id)
        {
            return Ok(_skillRepository.Remove(id));
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(Guid id, Skill skill)
        {
            return Ok(_skillRepository.Update(id, skill));
        }
    }
}
