﻿using Microsoft.AspNetCore.Mvc;
using Repositories.Entity;
using Repositories.Interfaces;
using Services.Security;
using System.Linq.Expressions;

namespace Project_C.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeJwt]
    public class DiplomaController : ControllerBase
    {
        private readonly IRepository<Diploma> _diplomaRepository;

        public DiplomaController(IRepository<Diploma> diplomaRepository)
        {
            _diplomaRepository = diplomaRepository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            return Ok(_diplomaRepository.Get(id));
        }

        [HttpGet("")]
        public IActionResult GetAll(string? name = null)
        {
            Expression<Func<Diploma, bool>> condition = x => true;

            if (name != null)
                condition = x => x.Name.Contains(name);

            return Ok(_diplomaRepository.GetAll(condition));
        }

        [HttpGet("count")]
        public IActionResult Count()
        {
            Expression<Func<Diploma, bool>> condition = x => true;

            return Ok(_diplomaRepository.Count(condition));
        }

        [HttpGet("Exist/{id}")]
        public IActionResult Exists(Guid id)
        {
            return Ok(_diplomaRepository.Exists(id));
        }

        [HttpPost("")]
        public IActionResult Add([FromForm] Diploma skill)
        {
            return Ok(_diplomaRepository.Add(skill));
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(Guid id)
        {
            return Ok(_diplomaRepository.Remove(id));
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(Guid id, Diploma skill)
        {
            return Ok(_diplomaRepository.Update(id, skill));
        }
    }
}
