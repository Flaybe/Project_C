﻿using Microsoft.AspNetCore.Mvc;
using Repositories.Entity.UtilityObject;
using Services.Interfaces;
using Services.Security;

namespace Project_C.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeJwt]
    public class FileController : ControllerBase
    {
        private readonly IFileService _fileService;

        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpPost("")]
        public IActionResult Add([FromForm] LocalFile fileDetails)
        {
            if (fileDetails == null)
                return BadRequest();

            var file = _fileService.PostFile(fileDetails.FileDetails, fileDetails.FileType);
            return Ok(file);
        }

        [HttpGet()]
        public IActionResult Get(Guid id)
        {            
            return Ok(_fileService.GetFileById(id));
        }
    }
}
