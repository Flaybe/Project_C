﻿using Microsoft.AspNetCore.Mvc;
using Repositories.Entity;
using Repositories.Interfaces;
using Repositories.Repositories;
using Services.Security;
using System.Linq.Expressions;

namespace Project_C.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeJwt]
    public class SkillCategoryController : ControllerBase
    {
        private readonly IRepository<SkillCategory> _skillCategoryRepository;

        public SkillCategoryController(IRepository<SkillCategory> skillCategoryRepository)
        {
            _skillCategoryRepository = skillCategoryRepository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var res = _skillCategoryRepository.Get(id);
            return Ok(res);
        }

        [HttpGet("")]
        public IActionResult GetAll(string? name = null)
        {
            Expression<Func<SkillCategory, bool>> condition = x => true;

            if (name != null)
                condition = x => x.Name.Contains(name);

            return Ok(_skillCategoryRepository.GetAll(condition));
        }

        [HttpGet("count")]
        public IActionResult Count()
        {
            Expression<Func<SkillCategory, bool>> condition = x => true;

            return Ok(_skillCategoryRepository.Count(condition));
        }

        [HttpGet("Exist/{id}")]
        public IActionResult Exists(Guid id)
        {
            return Ok(_skillCategoryRepository.Exists(id));
        }

        [HttpPost("")]
        public IActionResult Add([FromForm] SkillCategory skillCategory)
        {
            return Ok(_skillCategoryRepository.Add(skillCategory));
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(Guid id)
        {
            return Ok(_skillCategoryRepository.Remove(id));
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(Guid id, SkillCategory skillCategory)
        {
            return Ok(_skillCategoryRepository.Update(id, skillCategory));
        }
    }
}
