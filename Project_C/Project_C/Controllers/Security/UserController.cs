﻿using Microsoft.AspNetCore.Mvc;
using Services.Authentication;
using Services.Interfaces;

namespace Project_C.Controllers.Security
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpPost("login")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = _service.Authenticate(model);

            if (response == null)
                return BadRequest("Username or password is incorrect");

            return Ok(response);
        }

        [HttpPost("register")]
        public IActionResult Register(AuthenticateRequest model)
        {
            var response = _service.Register(model);

            if (response == null)
                return BadRequest("Problem creating new account");

            return Ok(response);
        }
    }
}
