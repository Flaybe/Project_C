﻿using Microsoft.AspNetCore.Mvc;

namespace Project_C.Controllers.Security
{
    [Route("healthcheck")]
    [ApiController]
    public class HealthCheckController : ControllerBase
    {
        [HttpGet]
        public IActionResult HealthCheck()
        {
            return Ok("Api fonctionne");
        }
    }
}
