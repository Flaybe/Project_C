﻿using Microsoft.AspNetCore.Mvc;
using Repositories.Entity;
using Repositories.Interfaces;
using Services.Security;
using System.Linq.Expressions;

namespace Project_C.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeJwt]
    public class PassionController : ControllerBase
    {
        private readonly IRepository<Passion> _passionRepository;

        public PassionController(IRepository<Passion> passionRepository)
        {
            _passionRepository = passionRepository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            return Ok(_passionRepository.Get(id));
        }

        [HttpGet("")]
        public IActionResult GetAll(string? title = null)
        {
            Expression<Func<Passion, bool>> condition = x => true;

            if (title != null)
                condition = x => x.Title.Contains(title);
            
            return Ok(_passionRepository.GetAll(condition));
        }

        [HttpGet("count")]
        public IActionResult Count()
        {
            Expression<Func<Passion, bool>> condition = x => true;

            return Ok(_passionRepository.Count(condition));
        }

        [HttpGet("Exist/{id}")]
        public IActionResult Exists(Guid id)
        {
            return Ok(_passionRepository.Exists(id));
        }

        [HttpPost("")]
        public IActionResult Add([FromForm]Passion passion)
        {
            return Ok(_passionRepository.Add(passion));
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(Guid id)
        {
            return Ok(_passionRepository.Remove(id));
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(Guid id, Passion passion)
        {
            return Ok(_passionRepository.Update(id, passion));
        }

    }
}
