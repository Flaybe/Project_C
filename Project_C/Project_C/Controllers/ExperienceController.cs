﻿using Microsoft.AspNetCore.Mvc;
using Repositories.Entity;
using Repositories.Interfaces;
using Repositories.Repositories;
using Services.Security;
using System.Linq.Expressions;

namespace Project_C.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeJwt]
    public class ExperienceController : ControllerBase
    {
        private readonly IRepository<Experience> _experienceRepository;

        public ExperienceController(IRepository<Experience> experienceRepository)
        {
            _experienceRepository = experienceRepository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            return Ok(_experienceRepository.Get(id));
        }

        [HttpGet("")]
        public IActionResult GetAll(string? name = null)
        {
            Expression<Func<Experience, bool>> condition = x => true;

            if (name != null)
                condition = x => x.CompanyName.Contains(name);

            return Ok(_experienceRepository.GetAll(condition));
        }

        [HttpGet("count")]
        public IActionResult Count()
        {
            Expression<Func<Experience, bool>> condition = x => true;

            return Ok(_experienceRepository.Count(condition));
        }

        [HttpGet("Exist/{id}")]
        public IActionResult Exists(Guid id)
        {
            return Ok(_experienceRepository.Exists(id));
        }

        [HttpPost("")]
        public IActionResult Add([FromForm] Experience skill)
        {
            return Ok(_experienceRepository.Add(skill));
        }

        [HttpDelete("{id}")]
        public IActionResult Remove(Guid id)
        {
            return Ok(_experienceRepository.Remove(id));
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(Guid id, [FromForm] Experience skill)
        {
            var test = _experienceRepository.Update(id, skill); //TODO BUG ON PATCH ID NOT FOUND
            return Ok(test);
        }
    }
}
