using Microsoft.EntityFrameworkCore;
using Project_C.DependencyInjector;
using Services.Security;
using Repositories;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
var di = new DependencyInjector();
var corsPolicy = "CorsPolicy";

var config = builder.Configuration;
var services = builder.Services;

di.InjectAll(services, config);
services.AddCors(options =>
{
    options.AddPolicy(corsPolicy, builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
});

services.AddControllers().AddNewtonsoftJson(
    options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);
services.AddControllers();

services.Configure<AppSettings>(config.GetSection("AppSettings"));
var cs = config.GetConnectionString("Project_C");
services.AddEntityFrameworkMySql().AddDbContext<CvDb>(opt =>
    opt.UseMySql(cs, ServerVersion.AutoDetect(cs)));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(option =>
{
    option.SwaggerDoc("v1", new OpenApiInfo { Title = "Project C Api", Version = "v1" });
    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },
            new string[]{}
        }
    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseHttpsRedirection();
app.UseCors(corsPolicy);
app.UseAuthorization();
app.UseMiddleware<JwtMiddleware>();
app.MapControllers();

using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetRequiredService<CvDb>();
    context.Database.Migrate();
}

app.Run();
