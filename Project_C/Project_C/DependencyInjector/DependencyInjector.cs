﻿using Microsoft.EntityFrameworkCore;
using Repositories;
using Repositories.Entity;
using Repositories.Entity.UtilityObject;
using Repositories.Interfaces;
using Repositories.Repositories;
using Services.Interfaces;
using Services.Services;
namespace Project_C.DependencyInjector;

public class DependencyInjector
{
    public void InjectAll(IServiceCollection services, IConfiguration configuration)
    {
        InjectServices(services);
        InjectRepositories(services);
    }

    public void InjectRepositories(IServiceCollection services)
    {
        services.AddScoped<DbContext, CvDb>();
        services.AddScoped<IRepository<User>, UserRepository>();
        services.AddScoped<IRepository<LocalFile>, FileRepository>();
        services.AddScoped<IRepository<Passion>, PassionRepository>();
        services.AddScoped<IRepository<SkillCategory>, SkillCategoryRepository>();
        services.AddScoped<IRepository<Skill>, SkillRepository>();
        services.AddScoped<ISkillRepository, SkillRepository>();
        services.AddScoped<IRepository<Diploma>, DiplomaRepository>();
        services.AddScoped<IRepository<Experience>, ExperienceRepository>();

    }

    public void InjectServices(IServiceCollection services)
    {
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IFileService, FileService>();
    }
}