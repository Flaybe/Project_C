﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Repositories.Entity;
using Repositories.Interfaces;
using Services.Authentication;
using Services.Interfaces;
using Services.Security;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Services.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly AppSettings _settings;

        public UserService(IRepository<User> userRepository, IOptions<AppSettings> appSettings)
        {
            _userRepository = userRepository;
            _settings = appSettings.Value;
        }

        public User? Get(Guid id)
        {
            return _userRepository.Get(id);
        }

        public User Register(AuthenticateRequest request)
        {
            var user = new User() { 
                Id = Guid.NewGuid(),
                Login = request.Login,
                Password = request.Password,
            };

            return _userRepository.Add(user);
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var users = _userRepository.GetAll(x => x.Login == model.Login);

            // return null if user not found
            if (users == null) return null;

            // check if password is correct
            User myUser = null;
            foreach (var user in users)
            {
                if (User.VerifyPassword(model.Password, user.Password))
                {
                    myUser = user;
                }
            }
            if (myUser == null) return null;

            // authentication successful so generate jwt token
            var token = GenerateJwtToken(myUser);

            return new AuthenticateResponse(myUser, token);
        }

        public string GenerateJwtToken(User user)
        {
            // generate token that is valid for 8 hours
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_settings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddHours(8),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
