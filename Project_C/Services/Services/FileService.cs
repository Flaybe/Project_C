﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Repositories.Entity.UtilityObject;
using Repositories.Enumerations;
using Repositories.Interfaces;
using Services.Interfaces;
using Services.Security;

namespace Services.Services
{
    public class FileService : IFileService
    {
        private readonly IRepository<LocalFile> _fileRepository;
        private readonly AppSettings _settings;

        public FileService(IRepository<LocalFile> fileRepository, IOptions<AppSettings> appSettings)
        {
            _fileRepository = fileRepository;
            _settings = appSettings.Value;
        }

        public LocalFile PostFile(IFormFile fileData, FileType fileType)
        {
            var localFile = new LocalFile()
            {
                FileName = fileData.FileName,
                FileType = fileType,
            };

            using (var stream = new MemoryStream())
            {
                fileData.CopyTo(stream);
                localFile.FileData = stream.ToArray();
            }

            return _fileRepository.Add(localFile);
        }

        public LocalFile GetFileById(Guid id)
        {
            return _fileRepository.Get(id); ;
        }
    }
}
