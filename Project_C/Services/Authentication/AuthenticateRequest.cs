﻿using System.ComponentModel.DataAnnotations;

namespace Services.Authentication
{
    public class AuthenticateRequest
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
