﻿using Repositories.Entity;
using System.Text.Json.Serialization;

namespace Services.Authentication
{
    public class AuthenticateResponse
    {
        public string Token { get; set; }
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public AuthenticateResponse(User client, string token)
        {
            Id = client.Id;
            Login = client.Login;
            Password = client.Password;
            Token = token;
        }

        [JsonConstructor]
        public AuthenticateResponse(string token, Guid id, string login, string password)
        {
            Token = token;
            Id = id;
            Login = login;
            Password = password;
        }
    }
}
