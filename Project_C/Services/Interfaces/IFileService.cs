﻿using Microsoft.AspNetCore.Http;
using Repositories.Entity.UtilityObject;
using Repositories.Enumerations;

namespace Services.Interfaces
{
    public interface IFileService
    {
        LocalFile PostFile(IFormFile fileData, FileType fileType);

        LocalFile GetFileById(Guid Id);
    }
}
