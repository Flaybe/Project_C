﻿using Repositories.Entity;
using Services.Authentication;

namespace Services.Interfaces
{
    public interface IUserService
    {
        User Register(AuthenticateRequest model);
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        User? Get(Guid id);
    }
}
